__author__ = 'demon'

from custom_xmi_parser import xmiparser as parser_1
from custom_xmi_parser import xmiparser_2 as parser_2

XMIModel = parser_2.parse(xschemaFileName='movies.xmi')
datatypes = XMIModel.get_datatypes()
classes = XMIModel.get_classes()
packages = XMIModel.get_packages()
association = XMIModel.get_association()
interfaces = XMIModel.get_interfaces()
print "List of Type: "
for key,datatype in datatypes.items():
    print datatype.getAttribute('name')

for key,classes in classes.items():
    print 'Class '+classes.get_model_name()
    print '\n'
    print 'List of Attributes'
    for key1, prop in classes.get_properties().items():
        print prop.get_model_name()
        if type(prop.get_type()) is unicode:
            print prop.get_type()
        else:
            prop.get_type().getAttribute('name')
        print prop.get_visibility()
        print '\n'

    print 'List of Attributes'
    for key1, prop in classes.get_operations().items():
        print prop.get_model_name()
        for key2, param in prop.get_parameters().items():
            print param.get_model_name()
            print param.get_type().getAttribute('name')
            out = ''
            if len(param.get_param_direction()) > 0:
                out = out + 'Direction '+param.get_param_direction()
            out = out + ' Lower: '+str(param.get_lower_value().get_value())+' Upper :'+str(param.get_upper_value().get_value())
            print out
            print '\n'

'''
print "\nList of Association: "
for key,assoc in association.items():
    print assoc.get_model_name()
    print 'Starting point'
    print assoc.get_from_to().get_type().getAttribute('name')
    print 'lower multiplicity '+str(assoc.get_from_to().get_lower_value().get_value())
    print 'upper multiplicity '+str(assoc.get_from_to().get_upper_value().get_value())
    print 'Ending point'
    print assoc.get_end_to().get_type().getAttribute('name')
    print 'lower multiplicity '+str(assoc.get_end_to().get_lower_value().get_value())
    print 'upper multiplicity '+str(assoc.get_end_to().get_upper_value().get_value())
    print ''
'''
'''for key,package in XMIModel.get_packages().items():
    print 'Name of the package is %s and Visibility is %s \n'%(package.get_model_name(),package.get_visibility())
    for key, kelas in package.get_classes().items():
        print kelas.get_package()
        print kelas.get_model_name()
        for key, att in kelas.get_attributes().items():
            print 'Datatype of this attribute is %s and name is %s and is it id ? %s'%(att.data_type, att.get_model_name(),att.get_is_id())
            print '\n'

    print 'Association: \n'
    for key,assoc in package.get_association().items():
        print assoc.get_from_to().get_aggregation_type()
        print assoc.get_end_to()
        
print XMIModel.classes
for key, class_model in XMIModel.classes.items():
    attributes = class_model.attributes
    operations = class_model.operations

    print 'Name of the class is '+class_model.get_model_name()+' and Visibility is '+ class_model.visibility + '\n'

    print 'Class Movies Attributes and its multiplicity\n'
    for key, att in attributes.items():
        print 'Datatype of this attribute is '+att.data_type
        print att.get_model_name()+' Lower Mult: '+ str(att.lower_multiplicity)+':'
        print att.get_model_name()+' Upper Mult: ' + str(att.upper_multiplicity)
        print '\n   '
    print '\n'
    print 'Class Movies Operation and its param\n'
    for key, op in operations.items():
        print op.get_model_name()
        for param in op.parameters:
            print param
'''
'''
from xml.dom import minidom

doc = minidom.parse('movies.core')
core =  doc.getElementsByTagName('core:IFMLModel')[0]

#Getting name
print 'Name of model is: '+core.getAttribute('name')

#Getting root of interaction flow model
interactionFlowModel = doc.getElementsByTagName('interactionFlowModel')

#Getting all og interaction flow model elements
interactionFlowModelElement = doc.getElementsByTagName('interactionFlowModelElements')

print 'List of all element :'
for element in interactionFlowModelElement:
    print element.getAttribute('name')
'''
