from utils import normalize


class NoObject(object):
    pass

_marker = NoObject()
allObjects = {}

def getElementsByTagName(domElement, tagName, recursive=0):
    """Returns elements by tag name.

    The only difference from the original getElementsByTagName is
    the optional recursive parameter.
    """
    if isinstance(tagName, basestring):
        tagNames = [tagName]
    else:
        tagNames = tagName
    if recursive:
        els = []
        for tag in tagNames:
            els.extend(domElement.getElementsByTagName(tag))
    else:
        els = [el for el in domElement.childNodes
               if str(getattr(el, 'tagName', None)) in tagNames]
    return els

def getElementByTagName(domElement, tagName, default=_marker, recursive=0):
    """Returns a single element by name and throws an error if more
    than one exists.
    """
    els = getElementsByTagName(domElement, tagName, recursive=recursive)
    if len(els) > 1:
        raise TypeError, 'more than 1 element found'
    try:
        return els[0]
    except IndexError:
        if default == _marker:
             raise
        else:
            return default

def hasClassFeatures(domClass):
    return len(domClass.getElementsByTagName(XMI.FEATURE)) or \
                len(domClass.getElementsByTagName(XMI.ATTRIBUTE)) or \
                len(domClass.getElementsByTagName(XMI.METHOD))

def getAttributeValue(domElement, tagName=None, default=_marker, recursive=0, doReplace=False):
    el = domElement
    #el.normalize()
    if tagName:
        try:
            el = getElementByTagName(domElement, tagName, recursive=recursive)
        except IndexError:
            if default == _marker:
                raise
            else:
                return default
    if el.hasAttribute('value'):
        return el.getAttribute('value')
    if not el.firstChild and default != _marker:
        return default

class XMI2_0(object):


    #Main XML Tag
    PACKAGED_ELEMENT = 'packagedElement' #Element in diagram
    OWNED_PARAMETER = 'ownedParameter' #Parameter in element
    PACKAGE_IMPORT = 'packageImport' #Imported Package in element
    OWNED_RULE = 'ownedRule' #Rule in element
    OWNED_COMMENT = 'ownedComment' #Comment for that element
    OWNED_OPERATION = 'ownedOperation' #Opertaion in that element
    MODEL = 'uml:Model'  # Model Tag in XMI UML
    GENERALIZATION = 'generalization' #Id of generalization element

    #XMI Attribute
    TYPE = 'xmi:type' #Accessing attribute Packaged Element Type
    NAME = 'name' #Name of that tag
    AGGREGATION = 'aggregation'

    #XMI Value
    PARAMETER = 'uml:Parameter'  # Parameter tag
    CONSTRAINT = 'uml:Constraint'  # Constraint tag
    CLASS = 'uml:Class'  # Class tag
    PACKAGE = 'uml:Package'  # Package tag
    PRIMITIVE_TYPE = 'uml:PrimitiveType'  # Creating a datatype
    OPERATION = 'uml:Operation'
    ASSOCIATION = 'uml:Association'
    DATA_TYPE = 'uml:DataType'
    INTERFACE = 'uml:Interface'
    ACTOR = "uml:Actor"
    STEREOTYPE = "uml:Stereotype"


    OWNED_ELEMENT = "UML:Namespace.ownedElement"



    # Collaboration
    COLLAB = 'Behavioral_Elements.Collaborations.Collaboration'


    # To match up a CR with the right start state, we look out for the context
    MULTIPLICITY = 'UML:StructuralFeature.multiplicity'
    ATTRIBUTE = 'UML:Attribute'
    DATATYPE = 'UML:DataType'
    FEATURE = 'UML:Classifier.feature'
    CLASSIFIER = 'UML:Classifier'

    ASSOCEND = 'UML:AssociationEnd'
    ASSOCENDTYPE = 'UML:AssociationEnd.type'
    ASSOCEND_PARTICIPANT = 'UML:AssociationEnd.participant'
    METHOD = "UML:Operation"
    METHODPARAMETER = "UML:Parameter"
    MULTRANGE = 'UML:MultiplicityRange'

    MULT_MIN = 'UML:MultiplicityRange.lower'
    MULT_MAX = 'UML:MultiplicityRange.upper'

    GEN_CHILD = "UML:Generalization.child"
    GEN_PARENT = "UML:Generalization.parent"
    GEN_ELEMENT = "UML:Class"

    ATTRIBUTE_INIT_VALUE = "UML:Attribute.initialValue"
    EXPRESSION = ["UML:Expression", "UML2:OpaqueExpression"]
    PARAM_DEFAULT = "UML:Parameter.defaultValue"

    TAG_DEFINITION = "UML:TagDefinition"

    TAGGED_VALUE_MODEL = "UML:ModelElement.taggedValue"
    TAGGED_VALUE = "UML:TaggedValue"
    TAGGED_VALUE_TAG = "UML:TaggedValue.tag"
    TAGGED_VALUE_VALUE = "UML:TaggedValue.value"

    MODELELEMENT = "UML:ModelElement"
    STEREOTYPE_MODELELEMENT = "UML:ModelElement.stereotype"

    STEREOTYPE = "UML:Stereotype"
    ISABSTRACT = "UML:GeneralizableElement.isAbstract"

    ABSTRACTION = "UML:Abstraction"

    DEPENDENCY = "UML:Dependency"
    DEP_CLIENT = "UML:Dependency.client"
    DEP_SUPPLIER = "UML:Dependency.supplier"

    ASSOCIATION_CLASS = 'UML:AssociationClass'
    BOOLEAN_EXPRESSION = ["UML:BooleanExpression", "UML2:OpaqueExpression"]

    # State Machine

    STATEMACHINE = "UML:StateMachine", "UML2:StateMachine"
    STATEMACHINE_CONTEXT = "UML:StateMachine.context"
    STATEMACHINE_TOP = "UML:StateMachine.top"
    COMPOSITESTATE = "UML:CompositeState"
    COMPOSITESTATE_SUBVERTEX = "UML:CompositeState.subvertex"
    SIMPLESTATE = "UML:SimpleState", "UML2:State"
    PSEUDOSTATE = "UML:Pseudostate", "UML2:PseudoState", "UML2:Pseudostate"
    PSEUDOSTATE_KIND = "kind"
    FINALSTATE = "UML:FinalState", "UML2:FinalState"
    STATEVERTEX_OUTGOING = "UML:StateVertex.outgoing", "UML2:Vertex.outgoing"
    STATEVERTEX_INCOMING = "UML:StateVertex.incoming", "UML2:Vertex.incoming"
    TRANSITION = "UML:Transition", "UML2:Transition"
    STATEMACHINE_TRANSITIONS = "UML:StateMachine.transitions"
    TRANSITON_TARGET = "UML:Transition.target", "UML2:Transition.target"
    TRANSITION_SOURCE = "UML:Transition.source", "UML2:Transition.source"
    TRANSITION_EFFECT = "UML:Transition.effect", "UML2:Transition.effect"
    TRANSITION_GUARD = "UML:Transition.guard", "UML2:Transition.guard"
    OWNED_BEHAVIOR = "UML2:BehavioredClassifier.ownedBehavior"

    ACTION_SCRIPT = "UML:Action.script", "UML2:OpaqueBehavior"
    ACTION_EXPRESSION = "UML:ActionExpression"
    ACTION_EXPRESSION_BODY = "UML:ActionExpression.body", "UML2:OpaqueBehavior.body"

    DIAGRAM = "UML:Diagram"
    DIAGRAM_OWNER = "UML:Diagram.owner"
    DIAGRAM_SEMANTICMODEL_BRIDGE = "UML:Uml1SemanticModelBridge"
    DIAGRAM_SEMANTICMODEL_BRIDGE_ELEMENT = "UML:Uml1SemanticModelBridge.element"


    UML2TYPE = 'UML2:TypedElement.type'

    def __init__(self,**kw):
        self.__dict__.update(kw)

    def getName(self, domElement, doReplace=False):
        name = getAttributeValue(domElement, self.NAME)
        return normalize(name, doReplace)

    def getId(self, domElement):
        return domElement.getAttribute('xmi.id').strip()

class XMI2_5(XMI2_0):

    '''
    def collectTagDefinitions(self, el, prefix=''):
        tagdefs = el.getElementsByTagName(self.TAG_DEFINITION)
        if self.tagDefinitions is None:
            self.tagDefinitions = {}
        for t in tagdefs:
            if t.hasAttribute('name'):
                self.tagDefinitions[prefix + t.getAttribute('xmi.id')] = t
    '''
