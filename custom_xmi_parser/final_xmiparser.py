class XMIClass(XMIElement):

    def __init__(self, *args, **kw):
        self._gen = {}
        self._package = None
        self._is_interface = 0
        self._is_abstract = 0
        self._is_leaf = 0
        self._visibility = 'public'
        self._attributes = {}
        self._operations = {}
        log.debug("Initialising class.")
        # ugh, setPackage(). Handle this with some more generic zope3
        # parent() relation. [reinout]
        log.debug("Running XMIClass's init...")
        self.model = kw.get('el')
        self.content = self.model.childNodes
        self.build_class()
        self.set_package(kw.get('package'))
        self._visibility = self.get_value_visibility(self.model) if self.get_value_visibility(self.model) != '' else 'public'
        self._is_abstract = 1 if self.set_abstract(self.model) == 'true' else 0
        self._is_leaf = 1 if self.set_leaf(self.model) == 'true' else 0

    def set_package(self, new_package):
        self._package = new_package

    def build_class(self):
        diagram_els = self.content
        for el in diagram_els:
            # try:
            if self.is_text_node(el):
                pass
            elif self.is_eannotation(el):
                pass
            else:
                if self.is_attribute(el):
                    self._attributes[str(el.getAttribute(XMI.ID))] = XMIAttribute(el=el)
                elif self.is_generalization(el):
                    self._gen[str(el.getAttribute(XMI.ID))] = str(self._set_generalization(el))
                elif self.is_operation(el):
                    self._operations[str(el.getAttribute(XMI.ID))] = XMIOperation(el=el)
                elif self.is_primitive_types(el) or self.is_data_types(el):
                    pass
                else:
                    raise ValueError, '%s, not recognized in UML Class Diagram \
                              ' % el.getAttribute('xmi:type')


    def is_attribute(self, node):
        if node.nodeType != node.TEXT_NODE and node.tagName == XMI.OWNED_ATTRIBUTE:
            return True
        else:
            return False

    def is_operation(self, node):
        if node.nodeType != node.TEXT_NODE and node.tagName == XMI.OWNED_OPERATION:
            return True
        else:
            return False

    def is_generalization(self, node):
        if node.nodeType != node.TEXT_NODE and node.tagName == XMI.GENERALIZATION:
            return True
        else:
            return False

    def _set_generalization(self, node):
        return node.getAttribute('general')

    def get_generalization(self):
        return self._gen

    def get_is_interface(self):
        return self._is_interface

    def get_visibility(self):
        return self._visibility

    def get_attributes(self):
        return self._attributes

    def get_operations(self):
        return self._operations

    def get_package(self):
        return self._package

    def set_abstract(self, node):
        return node.getAttribute(XMI.IS_ABSTRACT)

    def set_leaf(self, node):
        return node.getAttribute(XMI.IS_LEAF)