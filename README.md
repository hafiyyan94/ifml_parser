# UML Class Diagram Internal Representation

An Internal Representation of UML Class Diagram

* * *

## How to Use

1. Please use UML Class Diagram that have been made using Eclipse (Eclipse Modeling Framework), and take your \
Diagram (It is saved in `.uml` format)
1. Put the file in the same level of `test.py` file inside `uml_parser` package
1. Use this line of code as an example to start parsing your Class Diagram
    ```python
    from custom_xmi_parser import xmiparser_2 as parser_2
    
    XMIModel = parser_2.parse(xschemaFileName='sample (2).xmi')
    ```
1. If there's no error or exception, then your class diagram is already parsed

## Documentation

### xmiparser_2

All parsing method is implemented here. Here's the method that you can use for parsing your Class Diagram file:
1. parse(xschemaFileName=None):
    * xschemaFileName : insert your filename here
    * return : This method will return an XMIModel class that represent your Class Diagram

### List of Class

1. XMIModel
    1. Class Attribute
        * model = DOM Element that representing this Class
        * content = Child Element of Model. Representing the Content of this Class
        * classes = Dict of XMIClass inside XMIModel (It's representing a class that doesn't have a package)
        * packages = Dict of XMIPackage inside XMIModel 
        * interfaces = Dict of XMIInterfaces inside XMIModel
        * data_types = Dict of Datatypes or Primitivetypes that defined in XMIModel
        * association = Dict of XMIAssociation in XMIModel
    1. Class Method
        * get_classes = Getting Dict of XMIClass inside XMIModel
        * get_packages = Getting Dict of XMIPackage inside XMIModel 
        * get_interfaces = Getting Dict of XMIInterfaces inside XMIModel
        * get_datatypes = Getting Dict of Datatypes or Primitivetypes that defined in XMIModel
        * get_model_name = Getting the name of this Class (in this case, the name of UML Class Diagram Project)
        * get_association = Getting Dict of XMIAssociation in XMIModel
1. XMIPackage
    1. Class Attribute
        * model = DOM Element that representing this Class
        * content = Child Element of Model. Representing the Content of this Class
        * classes = Dict of XMIClass inside XMIPackage
        * packages = Dict of XMIPackage inside XMIPackage 
        * interfaces = Dict of XMIInterfaces inside XMIPackage
        * visibility = Visibility of this package
        * project = The name of root package oh this package
    1. Class Method
        * get_classes = Getting Dict of XMIClass inside XMIModel
        * get_packages = Getting Dict of XMIPackage inside XMIModel 
        * get_interfaces = Getting Dict of XMIInterfaces inside XMIModel
        * get_datatypes = Getting Dict of Datatypes or Primitivetypes that defined in XMIModel
        * get_model_name = Getting the name of this Class (in this case, Package Name)
        * get_association = Getting Dict of XMIAssociation in XMIModel
        * get_visibility = Getting Visibility of this package
        * get_project = Getting The name of root package oh this package
1. XMIClass
    1. Class Attribute
        * model = DOM Element that representing this Class
        * content = Child Element of Model. Representing the Content of this Class
        * gen = Dict that define genralization of this class
        * operations = Dict of XMIOperations in XMIClass
        * attributes = Dict of XMIAttributes in XMIClass
        * visibility = Visibility of this class
        * package = Name of the package that own this class
        * is_interface = Defining whether this class is Interface or not
    1. Class Method
        * get_model_name = Getting the name of this Class (in this case, the Class name)
        * get_operations = Getting Dict of XMIOperations in XMIClass
        * get_attributes = Getting Dict of XMIOperations in XMIClass
        * get_visibility = Getting Visibility of this class
        * get_package = Getting Name of the package that own this class
        * get_is_interface = Return a 0 if this class is an Interface, 1 if this class isn't an Interface
        * get_generalization = Return Dict that define genralization of this class
1. XMIInterface
    > This class inherit XMIClass, but the value of is_interface is 1
1. XMIOperation
    1. Class Attribute
        * model = DOM Element that representing this Class
        * content = Child Element of Model. Representing the Content of this Class
        * parameters = Dict of XMIParameter in XMIOperation
        * visibility = Visibility of this operation
    1. Class Method
        * get_parameters = Getting Dict of XMIParameter in XMIOperation
        * get_model_name = Getting the name of this Class (in this case, the Operation name)
        * get_visibility = Getting Visibility of this operation
1. XMIParameter
    1. Class Attribute
        * model = DOM Element that representing this Class
        * content = Child Element of Model. Representing the Content of this Class
        * type = Datatype or Primitivetype of this Parameter
    1. Class Method
        * get_type = Getting Datatype or Primitivetype of this Parameter
        * get_model_name = Getting the name of this Class (in this case, the Parameter name)
1. XMIAttribute
    1. Class Attribute
        * model = DOM Element that representing this Class
        * content = Child Element of Model. Representing the Content of this Class
        * visibility = Visibility of this operation
        * lower_multiplicity = Defining Lower Multiplicity value that represented in XMIMultiplicity
        * upper_multiplicity = Defining Upper Multiplicity value that represented in XMIMultiplicity
    1. Class Method
        * get_visibility = Getting Visibility of this operation
        * get_lower_mult = Getting XMIMultiplicity that Define Lower Multiplicity value
        * get_upper_mult = Getting XMIMultiplicity that Define Upper Multiplicity value
        * get_model_name = Getting the name of this Class (in this case, the Class name)
1. XMIMultiplicity
    1. Class Attribute
        * model = DOM Element that representing this Class
        * type = datatype that represent a value of multiplicity
        * value = value of this multiplicity
    1. Class Method
        * get_mult_type = Getting datatype that represent a value of multiplicity
        * get_value = value of this multiplicity
1. XMIAssociation
    1. Class Attribute
        * model = DOM Element that representing this Class
        * content = Child Element of Model. Representing the Content of this Class
        * from_to = id of Class that involved in this association (source)
        * end_to = id of Class that involved in this association (target)
        * owned_end_desc = Dict of XMIOwnedEnd that Describing all of association member (From to Class, and End to Class)
    1. Class Method
        * get_model_name = Getting the name of this Class (in this case, the Class name)
        * get_from_to = Getting XMIOwnedEnd that describe association rule for this class (source)
        * get_end_to = Getting XMIOwnedEnd that describe association rule for this class (target)
1. XMIOwnedEnd
    1. Class Attribute
        * model = DOM Element that representing this Class
        * lower_multiplicity = Defining Lower Multiplicity value that represented in XMIMultiplicity
        * upper_multiplicity = Defining Upper Multiplicity value that represented in XMIMultiplicity
        * aggregation_type = Aggregation Type of this Class Association (None, Shared, Composite)
        * type = id of Class that involved in some association
    1. Class Method
        * get_type = Getting id of Class that involved in some association
        * get_lower_mult = Getting XMIMultiplicity that Define Lower Multiplicity value
        * get_upper_mult = Getting XMIMultiplicity that Define Upper Multiplicity value
        * get_aggregation_type = Getting Aggregation Type of this Class Association (None, Shared, Composite)